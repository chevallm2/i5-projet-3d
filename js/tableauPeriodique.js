const Element = require('./Element')
const THREE = require('three')
const TP = require('periodic-table')
const config = require('./configuration')

module.exports = class TableauPeriodique {

    /**
     * 
     * @param {Element[]} elements 
     */
    constructor() {
        this.elements = TP.all().sort( (a,b) => { return a.atomicNumber - b.atomicNumber });
        this.usableElements = [];
        this.group = new THREE.Group();
    }

    /**
     * Generate a group with all the elements in the periodique table
     */
    generateTableauPeriodique() {
        this.elements = this.elements.map( element => {
            let object = {
                nom: element.symbol,
                nomChimique: element.name,
                masseAtomique: element.atomicMass,
                atomicNumber: element.atomicNumber,
                groupe: element.groupBlock
            };
            this.usableElements[element.atomicNumber] = object;
            return object
        });

        let ordres = config.dispositionTableauPeriodique;

        this.elements.forEach( (element, index) => {
            const e = new Element(element.nom, element.nomChimique, element.atomicNumber, element.groupe);
            this.group.add(
                e.generateTuile(
                    (ordres[index].split(':')[0] + 1),
                    -(ordres[index].split(':')[1] + 1)
                )
            )
        })
    }

    /**
     * Get the Three JS Scene
     */
    getScene() {
        const scene = new THREE.Scene();
        this.generateTableauPeriodique();
        scene.add(this.group);
        return scene;
    }
}

