const AbstractPhysicObject = require('./AbstractPhysicObject');

module.exports = class Neutron extends AbstractPhysicObject {

    constructor() {
        super();
        this.mass = 1;
        this.outerRadius = 4;
        this.radius = 1;
        this.color = 0x55dddd;
        this.specular = 0x999999;
        this.shininess = 13;
    }

};