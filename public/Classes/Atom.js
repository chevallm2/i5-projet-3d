const Nucleus = require('./Nucleus');
const Electron = require('./Electron');
const Level = require('./Level');

module.exports = class Atom {
    constructor(atomicNumber, atomicMass) {
        let thresholds = [2, 8, 18, 32, 32, 18, 8];
        this.atomicNumber = atomicNumber;
        this.atomicMass = atomicMass;
        this.neutronsNumber = 0;
        this.nucleus = new Nucleus(this.atomicNumber);
        this.electrons = [];
        this.levels = thresholds.map((threshold, index) => {
            return new Level(threshold, index);
        });


        this.computeLevels();
        this.computeElectrons();
        this.computeNeutrons();
        this.computeProton();

    }

    computeLevels() {
        let decrement = this.atomicNumber;
        this.levels.forEach(
            (level) => {
                decrement = decrement - level.threshold;
                if (decrement >= 0) {
                    level.fillElectrons(level.threshold);
                }
                if (decrement < 0) {
                    level.fillElectrons(decrement + level.threshold);
                }
            }
        );
    }

    computeProton() {
        this.nucleus.computeProtons(this.atomicNumber);
    }

    computeNeutrons() {
        this.neutronsNumber = this.atomicMass - this.atomicNumber;
        this.nucleus.computeNeutrons(this.neutronsNumber);
    }

    computeElectrons() {
        for (let i = 0; i < this.atomicNumber; i++) {
            this.electrons.push(new Electron());
        }
    }
};