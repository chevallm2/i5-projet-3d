const AbstractPhysicObject = require('./AbstractPhysicObject');

module.exports = class Electron extends AbstractPhysicObject {

    constructor() {
        super();
        this.mass = 0.75;
        this.outerRadius = 25;
        this.radius = 0.2;
        this.color = 0xdddd55;
        this.specular = 0x999999;
        this.shininess = 13;
    }
};