const Proton = require('./Proton');
const Neutron = require('./Neutron');

module.exports = class Nucleus {
    constructor() {
        this.protons = [];
        this.neutrons = [];
    }

    computeProtons(atomicNumber) {
        for (let i = 0; i < atomicNumber; i++) {
            this.protons.push(new Proton());
        }
    }

    computeNeutrons(protonNumber) {
        for (let i = 0; i < protonNumber; i++) {
            this.neutrons.push(new Neutron());
        }
    }
};