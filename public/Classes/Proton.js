const AbstractPhysicObject = require('./AbstractPhysicObject');

module.exports = class Proton extends AbstractPhysicObject {

    constructor() {
        super();
        this.mass = 1;
        this.outerRadius = 4;
        this.radius = 1;
        this.color = 0xdd5555;
        this.specular = 0x999999;
        this.shininess = 13;
    }

};