const Electron = require('./Electron');

module.exports = class Level {
    constructor(threshold, index) {
        this.threshold = threshold;
        this.index = index;
        this.electrons = [];
        this.electronsMeshes =[];
    }

    fillElectrons(electronNumber) {
        for (let i = 0; i < electronNumber; i++) {
            let electron = new Electron();
            this.electrons.push(electron);
            this.electronsMeshes.push(electron.render())
        }
    }
};