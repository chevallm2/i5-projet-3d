const THREE = require('three');
const CANNON = require('cannon');

module.exports = class AbstractPhysicObject {
    constructor() {
        this.mass = 0;
        this.outerRadius = 10;
        this.radius = 0;
        this.color = 0xdddd55;
        this.specular = 0x999999;
        this.shininess = 13;
    }

    render() {
        return {
            // Cannon
            body: new CANNON.Body({
                mass: this.mass, // kg
                position: this.randomPosition(),
                shape: new CANNON.Sphere(this.radius)
            }),
            // THREE
            mesh: new THREE.Mesh(
                new THREE.SphereGeometry( this.radius, 32, 32 ),
                new THREE.MeshPhongMaterial( { color: this.color, specular: this.specular, shininess: this.shininess} )
            )
        }
    }

    randomPosition(){
        let x = (2 * Math.random() - 1 ) * this.outerRadius,
            y = (2 * Math.random() - 1 ) * this.outerRadius,
            z = (2 * Math.random() - 1 ) * this.outerRadius;
        return new CANNON.Vec3(x, y, z);
    }
};