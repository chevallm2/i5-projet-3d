const Groupe = require('./Groupe');
const THREE = require('three');

module.exports = class Element {
    
    /**
     * 
     * @param {string} nom 
     * @param {string} nomChimique 
     * @param {number} atomicNumber 
     * @param {Groupe} groupe 
     */
    constructor(nom, nomChimique, atomicNumber, groupe) {
        this.nom = nom;
        this.nomChimique = nomChimique;
        this.atomicNumber = atomicNumber;
        this.groupe = groupe;
    }

    generateTuile(x, y) {
        const elementGroup = new THREE.Group();

        new THREE.TextureLoader().load(`../ressources/elements/fff&text=${this.nom}.png`, texture => {
            const geometryFacette = new THREE.PlaneGeometry(8, 8);
            const materialFacette = new THREE.MeshBasicMaterial({ transparent: 0.5, map: texture});
            const plane = new THREE.Mesh(geometryFacette, materialFacette);
            plane.name = this.atomicNumber;
            plane.position.set(x - 75, y + 45, 0);
            
            elementGroup.add(plane)
        });
        
        return elementGroup
    }
};