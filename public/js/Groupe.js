module.exports = class Groupe {

    /**
     * 
     * @param {string} nom 
     * @param {string} couleur 
     */
    constructor(nom, couleur) {
        this.nom = nom
        this.couleur = couleur
    }
}