const THREE = require('three');
const tableauPeriodique = require('./js/tableauPeriodique');
const AtomicScene = require('./Scenes/AtomicScene');
const Atom = require('./Classes/Atom');
const AtomScene = require('./Classes/AtomScene');

// Initialisation
const scenes = []; // Un tableau de scene à rendre
let positionAtom = false;
let controls = null;
const cameraPrincipale = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 0.1, 1000);
cameraPrincipale.position.setZ(200);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

let mouse = new THREE.Vector2();

const _tableauPeriodique = new tableauPeriodique();
const sceneTableauPeriodique = _tableauPeriodique.getScene();
// sceneTableauPeriodique.background = new THREE.CubeTextureLoader()
//     .setPath('/ressources/cubetexture/')
//     .load([
//         '1.jpg',
//         '2.jpg',
//         '3.jpg',
//         '4.jpg',
//         '5.jpg',
//         '6.jpg'
//     ]);
// .load([
//         'posx.jpg',
//         'negx.jpg',
//         'posy.jpg',
//         'negx.jpg',
//         'posz.jpg',
//         'negz.jpg',
//     ]);
scenes.push(sceneTableauPeriodique);

function onClickOnElementFromPeriodiqueTable(element) {
    const elementMass = element.masseAtomique;
    const atomicMass = Array.isArray(elementMass) ? elementMass[0] : elementMass.substr(0, elementMass.length - 3);

    const atom = new Atom(element.atomicNumber, parseInt(atomicMass));
    const atomicScene = new AtomicScene(atom);
    scenes.push(atomicScene);
    // rotation de la camera à
    cameraPrincipale.lookAt(atomicScene.scene.position);
    positionAtom = true;

    // controls = new THREE.OrbitControls(cameraPrincipale, renderer.domElement);
    // controls.minDistance = 2;
    // controls.maxDistance = 30;
}

/**
 *
 * @param {MouseEvent} event
 */
function updateMouseCoordonates(event) {
    let domRect = renderer.domElement.getBoundingClientRect();
    mouse.setX(
        (event.clientX / window.innerWidth) * 2 - 1 + domRect.left
    );
    mouse.setY(
        -(event.clientY / window.innerHeight) * 2 + 1 + domRect.top
    );
}

const raycaster = new THREE.Raycaster();

function getElementClicked() {
    raycaster.setFromCamera(mouse, cameraPrincipale);
    const elements = _tableauPeriodique.group.children;
    const intersects = raycaster.intersectObjects(elements, true);

    return intersects[0];
}

document.addEventListener('mousedown', ev => {
    mouse = new THREE.Vector2();
    updateMouseCoordonates(ev);
    const elementClicked = getElementClicked();
    if (elementClicked !== undefined) {
        onClickOnElementFromPeriodiqueTable(_tableauPeriodique.usableElements[elementClicked.object.name])
    }
});

document.addEventListener('keyup', event => {
    if (!positionAtom) {
        return;
    }
    scenes.length = 1;
    cameraPrincipale.lookAt(scenes[0].position);
    positionAtom = false;
    controls = null;
});
scenes.forEach(scene => {
    // Lumière ambiante
    const light = new THREE.AmbientLight(0x202020);
    scene.add(light);

    // Lumière plafonier
    const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
    directionalLight.position.set(-1, 1, 1);
    scene.add(directionalLight);
});

function animate() {
    requestAnimationFrame(animate);
    let autoClear = true;
    scenes.forEach(scene => {
        if (scene instanceof AtomicScene) {
            autoClear = false;
            scene.render();
            scene = scene.scene;
            if (controls !== null) {
                controls.update();
            }
        }
        renderer.autoClear = autoClear;
        renderer.render(scene, cameraPrincipale)
    })
}

animate();