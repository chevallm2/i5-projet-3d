const CANNON = require('cannon');
const THREE = require('three');
const AtomScene = require('../Classes/AtomScene');
module.exports = class AtomicScene {

    constructor(atom) {
        this.atom = atom;
        this.protons = [];
        this.neutrons = [];
        this.electrons = [];
        this.atomGroup = new THREE.Group();
        this.scene = null;
        this.world = null;
        this.timeStep = 1 / 60;
        this.generateScene();
    }

    generateScene() {
        this.scene = new AtomScene();
        this.scene.position.z = 250;
        const light = new THREE.AmbientLight( 0x404040, 2 ); // soft white light
        this.scene.add( light );
        this.world = new CANNON.World();


        this.fillElectrons();
        this.fillNeutrons();
        this.fillProtons();

        this.electrons.forEach((electron) => this.addToWorld(electron));
        this.neutrons.forEach((neutron) => this.addToWorld(neutron));
        this.protons.forEach((proton) => this.addToWorld(proton));
        this.addToScene();
        //Small impulse on the electrons to get them moving in the start
        this.electrons.forEach((electron) => {
            let centerDir = electron.body.position.vsub(new CANNON.Vec3(0, 0, 0));
            centerDir.normalize();
            let impulse = centerDir.cross(new CANNON.Vec3(0, 0, 1.25));
            impulse.scale(2, impulse);
            electron.body.applyLocalImpulse(impulse, new CANNON.Vec3(0, 0, 0));
        });
    }

    render() {
        // all particles pull towards the center
        this.protons.forEach(this.pullOrigin);
        this.neutrons.forEach(this.pullOrigin);
        this.electrons.forEach(this.pullOrigin);

        this.electrons.forEach((electron) => {
            let pushForce = new CANNON.Vec3(0, 0, 0);

            this.protons.forEach((proton) => {
                let f = electron.body.position.vsub(proton.body.position);
                pushForce.vadd(f, pushForce);
            });

            this.neutrons.forEach((neutron) => {
                let f = electron.body.position.vsub(neutron.body.position);
                pushForce.vadd(f, pushForce);
            });

            pushForce.scale(0.07, pushForce);
            electron.body.force.vadd(pushForce, electron.body.force);
        });

        // protons and neutrons slows down (like wind resistance)
        this.neutrons.forEach((neutron) => this.resistance(neutron, 0.95));
        this.protons.forEach((proton) => this.resistance(proton, 0.95));

        // Electrons have a max velocity
        this.electrons.forEach((electron) => {
            this.maxVelocity(electron, 5)
        });

        // Step the physics world
        this.world.step(this.timeStep);
        // Copy coordinates from Cannon.js to Three.js
        this.protons.forEach((proton) => this.updateMeshState(proton));
        this.neutrons.forEach((neutron) => this.updateMeshState(neutron));
        this.electrons.forEach((electron) => this.updateMeshState(electron));

    }

    fillElectrons() {
        this.electrons = this.atom.electrons.map((electron) => electron.render());
    }

    fillNeutrons() {
        this.neutrons = this.atom.nucleus.neutrons.map((neutron) => neutron.render());
    }

    fillProtons() {
        this.protons = this.atom.nucleus.protons.map((proton) => proton.render());
    }

    addToWorld(object) {
        this.atomGroup.add(object.mesh);
        // this.scene.add(object.mesh);
        this.world.add(object.body);
    }

    addToScene(){
        this.scene.add(this.atomGroup);
    };

    pullOrigin(object) {
        object.body.force.set(
            -object.body.position.x,
            -object.body.position.y,
            -object.body.position.z
        );
    }

    updateMeshState(object) {
        object.mesh.position.copy(object.body.position);
        object.mesh.quaternion.copy(object.body.quaternion);
    }

    maxVelocity(object, vel) {
        if (object.body.velocity.length() > vel) {
            object.body.force.set(0, 0, 0);
        }
    }

    resistance(object, val) {
        if (object.body.velocity.length() > 0) {
            object.body.velocity.scale(val, object.body.velocity);
        }
    }
};